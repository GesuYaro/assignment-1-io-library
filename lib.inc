section .text
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60 
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .new_char:
        cmp byte [rdi + rax], 0
        jz .success
        inc rax
        jmp .new_char
    .success:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10
    mov rcx, rsp ; saving stack-pointer
    ; push 0 ; null-terminator
    dec rsp
    mov byte [rsp], 0
    .div_loop:
        xor rdx, rdx
        div rdi
        add dl, 0x30
        ; push rdx
        dec rsp
        mov byte [rsp], dl
        cmp rax, 0
        jne .div_loop
    
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    mov rsp, rcx ; ^ 
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jns .print_digits
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print_digits:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: 
    .loop:
        mov al, byte[rdi]
        cmp al, byte[rsi]
        jne .not_equal
        cmp al, 0
        je .equal
        inc rdi
        inc rsi
        jmp .loop
    .not_equal:
        xor rax, rax
        ret
    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера(rdi), размер буфера(rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    push rsi
    mov rcx, rsi
    .whitespace_skipping_loop:
        push rdi       ; reading new char
        push rcx
        call read_char
        pop rcx
        pop rdi

        cmp al, 0x20                    ; checking for whitespace
        je .whitespace_skipping_loop
        cmp al, 0x9
        je .whitespace_skipping_loop
        cmp al, 0xA
        je .whitespace_skipping_loop

    .main_loop:
        cmp rcx, 1 ; end of buffer (1 for null-terminator)
        jle .fail
        cmp rax, 0 ; end of input
        je .success

        mov byte [rdi], al ; writing char to buffer
        inc rdi ; change the next char index
        dec rcx ; dec the available buffer space

        push rdi        ; reading new char
        push rcx
        call read_char
        pop rcx
        pop rdi

        cmp al, 0x20   ; checking for whitespace, if reached, word is ended
        je .success
        cmp al, 0x9
        je .success
        cmp al, 0xA
        je .success

        jmp .main_loop

    .success:
        pop rdx ; initial buffer size
        pop rax ; initial buffer address
        sub rdx, rcx ; word_length = buffer_size - available_space
        mov byte [rdi], 0 ; adding a null-terminator
        ret
            
    .fail:
        pop rdx
        pop rax
        xor rax, rax    
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; number
    xor rdx, rdx 
    xor rcx, rcx ; length counter
    mov rsi, 10 ; needed to mul the rax
    .loop:
        xor r10, r10        ; taking one symbol
        mov r10b, byte [rdi]

        cmp r10b, 0x30 ; checking for a digit
        jl .end
        cmp r10b, 0x39
        jg .end

        inc rcx ; length++
        inc rdi ; pointer to the next symbol
        sub r10b, 0x30 ; ascii to digit
        mul rsi ; shift rax to put a new digit
        add rax, r10 ; putting a new digit

        jmp .loop

    .end:
        mov rdx, rcx ; length to rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx

    xor r10, r10
    mov r10b, byte [rdi]

    cmp r10b, 0x2d ; check for a minus
    je .negative
    jne .positive_or_nan

    .positive_or_nan:
        call parse_uint
        ret
    
    .negative:
        inc rdi ; for a minus
        call parse_uint
        inc rdx ; for a minus
        neg rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi

    cmp rdx, rax ; checking the buffer size
    jl .fail
    .loop:
        xor r10, r10
        mov r10b, byte[rdi] ; copying char
        mov byte[rsi], r10b ; to the new place
        cmp r10, 0 ; checking for null-terminator
        je .end
        inc rdi ; incrementing pointers
        inc rsi
        jmp .loop

    .fail:
        xor rax, rax
    .end:
        ret